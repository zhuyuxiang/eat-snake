import Food from './food'
import Score from './score'
import Snake from './snake'
class GameControl {
  food: Food
  score: Score
  snake: Snake
  direction: string = ''
  isLive: boolean = true
  constructor() {
    this.food = new Food()
    this.score = new Score()
    this.snake = new Snake()
    this.init()
  }
  init() {
    document.addEventListener('keydown', this.getDirection.bind(this))
    this.run()
  }
  getDirection(event: KeyboardEvent) {
    console.log(event.key)
    this.direction = event.key
  }
  run() {
    let X = this.snake.X
    let Y = this.snake.Y
    switch (this.direction) {
      case 'ArrowRight':
      case 'Right':
        X += 10
        break
      case 'ArrowLeft':
      case 'Left':
        X -= 10
        break
      case 'ArrowUp':
      case 'Up':
        Y -= 10
        break
      case 'ArrowDown':
      case 'Down':
        Y += 10
        break
    }
    this.eatCheck(X,Y)
    try {
      this.snake.X = X
      this.snake.Y = Y
    } catch (e) {
      alert(e)
      this.isLive = false
    }
    this.isLive && setTimeout(this.run.bind(this), 100 - this.score.level * 30)
  }
  //蛇吃到食物的逻辑
  eatCheck(X:number,Y:number){
    if(X === this.food.X && Y === this.food.Y){
      this.food.change()
      this.snake.addBody()
      this.score.addScore()
    }
  }
}
export default GameControl
