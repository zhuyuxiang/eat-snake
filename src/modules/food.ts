"use strict";
 class Food {
  element:HTMLElement;
  constructor(){
    this.element = document.getElementById('food')!;
    this.change()
  }
  //定义一个获取食物xy轴坐标的方法
  get X(){
    return this.element.offsetLeft
  }
  get Y(){
    return this.element.offsetTop
  }
  change(){
    //要求食物坐标必须是10的倍数,否则蛇一次走10,就吃不到了.食物区域水平是750
    this.element.style.left = Math.round(Math.random()*75)*10 + 'px'
    this.element.style.top = Math.round(Math.random()*59)*10 + 'px'
  }
}
export default Food