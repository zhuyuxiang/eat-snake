class Score {
  score=0;
  level=1;
  scoreEle:HTMLElement;
  levelEle:HTMLElement;
  MaxLevel:number;//最高等级
  upScore:number;//多少分升一级
  constructor(MaxLevel:number=10,upScore:number=10){
    this.MaxLevel = MaxLevel
    this.upScore = upScore
    this.scoreEle = document.getElementById('score')!
    this.levelEle = document.getElementById('level')!
  }
  addScore(){
    this.scoreEle.innerHTML = ++this.score + ''
    if(this.score % this.upScore === 0){
      this.levelUp()
    }
  }
  levelUp(){
    if(this.level<this.MaxLevel){
      this.levelEle.innerHTML = ++this.level + ''
    }
  }
}

export default Score