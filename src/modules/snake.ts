class Snake {
  element:HTMLElement;
  head:HTMLElement;//蛇头
  bodies:HTMLCollection;//s蛇身体,这个HTMLCollection会自动补充新元素
  constructor(){
    this.element = document.getElementById('snake')!;
    this.head = document.querySelectorAll('#snake>div')![0] as HTMLElement;
    this.bodies = this.element.getElementsByTagName('div')
  }
  // 蛇头的获取和设置位置
  get X(){
    return this.head.offsetLeft
  }
  get Y(){
    return this.head.offsetTop
  }

  set X(value:number){
    if(this.X === value){
      return
    }
    if(value<0 || value>750){
      throw new Error('蛇撞墙了,游戏结束')
    }
    if(this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value){
          if(value > this.X){
            value = this.X - 10
          } else {
            value = this.X + 10
          }
    }
    this.moveBody()
    
    this.head.style.left = value + 'px'
    this.checkBody()
  }
  set Y(value:number){
    if(this.Y === value){
      return
    }
    if(value<0 || value>590){
      throw new Error('蛇撞墙了,游戏结束')
    }
    if(this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value){
      if(value > this.Y){
        value = this.Y - 10
      } else {
        value = this.Y + 10
      }
}
    this.moveBody()
    this.head.style.top = value + 'px'
    this.checkBody()
  }
  // 蛇增加身体
  addBody(){
    this.element.insertAdjacentHTML("beforeend","<div></div>")
  }
  //蛇移动身体
  moveBody(){
    for(let i = this.bodies.length-1;i>0;i--){
     let X = (this.bodies[i-1] as HTMLElement).offsetLeft;
      let Y = (this.bodies[i-1] as HTMLElement).offsetTop;
      (this.bodies[i] as HTMLElement).style.left = X + 'px';
      (this.bodies[i] as HTMLElement).style.top = Y + 'px';
    }

  }
  //检查是否撞到自己
  checkBody(){
    for(let i = this.bodies.length-1;i>0;i--){
      if(this.X === (this.bodies[i] as HTMLElement).offsetLeft && this.Y === (this.bodies[i] as HTMLElement).offsetTop) {
        throw new Error('撞到自己了')
      }
     }
  }
}
export default Snake